package com.cvortex.dataschema.tenancy.repository;

import com.cvortex.dataschema.tenancy.model.Tenancy;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class TenacyTemplateRepository {

    private final MongoTemplate mongoTemplate;

    public List<Tenancy> findAll() {
        return mongoTemplate.findAll(Tenancy.class);
    }
}
