package com.cvortex.dataschema.tenancy.repository;

import com.cvortex.dataschema.tenancy.model.Tenancy;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TenancyRepository extends MongoRepository<Tenancy, String> {
}
