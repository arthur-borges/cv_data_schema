package com.cvortex.dataschema.tenancy.model;


public enum Types {
    CPF(
            Type.builder()
                    .nativeType(NativeType.TEXT)
                    .mask("999.999.999-99")
                    .validation("???") //TODO O IDEAL SERIA TEM UMA BiFunction AQUI
            .build()
    ),

    TEXTO(
            Type.builder().nativeType(NativeType.TEXT).build()
    ),

    BOOLEAN (
            Type.builder().nativeType(NativeType.BOOLEAN).build()
    );

    private Type type;

    Types(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
