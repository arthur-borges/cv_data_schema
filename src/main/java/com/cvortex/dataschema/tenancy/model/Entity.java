package com.cvortex.dataschema.tenancy.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Entity {
    private String uuid;
    private String name;
    private List<Field> fields;
    private List<List<String>> uniqueFields;
    private List<FieldReference> fieldsReference;
}
