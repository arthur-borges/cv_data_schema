package com.cvortex.dataschema.tenancy.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Model {
    private List<Entity> entities;
    private Version version;
}
