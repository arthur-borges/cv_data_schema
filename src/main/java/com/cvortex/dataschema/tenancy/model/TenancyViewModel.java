package com.cvortex.dataschema.tenancy.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class TenancyViewModel implements Serializable {

    private String id;
    private String name;

    public static TenancyViewModel of(Tenancy tenancy) {
        return TenancyViewModel
                .builder()
                .id(tenancy.getId())
                .name(tenancy.getName())
                .build();
    }
}
