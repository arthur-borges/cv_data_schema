package com.cvortex.dataschema.tenancy.model;

public enum NativeType {
    INT,TEXT,BOOLEAN
}
