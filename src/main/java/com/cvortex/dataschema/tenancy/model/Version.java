package com.cvortex.dataschema.tenancy.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Version {
    private Integer major;
    private Integer minor;
    private Integer build;
    private Integer revision;
    private VersionType versionType;
}
