package com.cvortex.dataschema.tenancy.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PoliceType {
    PII(() -> {
        System.out.println("model storage of pii is encrypted");
    }),
    PCI(() -> {
        System.out.println("model storage of pci is out of my domain");
    });

    private final StorageModel storageModel;
}
