package com.cvortex.dataschema.tenancy.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class FieldReference {
    private String entityTarget;
    private String fieldTarget;
    private Serializable value;
    private boolean required;
}
