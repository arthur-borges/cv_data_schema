package com.cvortex.dataschema.tenancy.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@Document
public class Tenancy {

    @Id
    private String id;

    private String name;

    private List<Model> models;

    @Version
    private Long version;
}
