package com.cvortex.dataschema.tenancy.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class Field {
    private String uuid;
    private String name;
    private int size;
    private boolean pci;
    private boolean pii;
    private boolean required;
    private Type type;
    private Entity entity;
    private Serializable defaultValue;
    private List<Serializable> acceptableValue;
}
