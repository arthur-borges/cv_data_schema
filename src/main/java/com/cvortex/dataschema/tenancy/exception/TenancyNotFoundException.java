package com.cvortex.dataschema.tenancy.exception;

public class TenancyNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    private final String msg;

    public TenancyNotFoundException(String message) {
        super(message);
        this.msg = message;
    }

    public String getMsg() {
        return this.msg;
    }
}
