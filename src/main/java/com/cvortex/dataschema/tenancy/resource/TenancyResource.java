package com.cvortex.dataschema.tenancy.resource;

import com.cvortex.dataschema.tenancy.exception.TenancyNotFoundException;
import com.cvortex.dataschema.tenancy.model.TenancyViewModel;
import com.cvortex.dataschema.tenancy.service.TenancyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/tenancy")
@RestController
@RequiredArgsConstructor
@Slf4j
public class TenancyResource {

    private final TenancyService tenancyService;

    @PostMapping
    public TenancyViewModel save(@RequestBody TenancyViewModel tenancyViewModel) {
        return tenancyService.save(tenancyViewModel);
    }

    @GetMapping("/all")
    public List<TenancyViewModel> all() {
        log.info("Get all....");
        return tenancyService.findAll();
    }

    @GetMapping("/{tenancyId}")
    public TenancyViewModel tenancyById(@PathVariable String tenancyId) {
        log.info("Getting tenacy....");
        return tenancyService.findById(tenancyId);
    }

    @GetMapping("/deleteCache")
    public String deleteCache() {
        return this.tenancyService.deleteAllCache();
    }
}
