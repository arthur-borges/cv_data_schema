package com.cvortex.dataschema.tenancy.service;

import com.cvortex.dataschema.tenancy.model.TenancyViewModel;

import java.util.List;

public interface TenancyService {

    TenancyViewModel save(TenancyViewModel tenancyViewModel);

    TenancyViewModel update(TenancyViewModel tenancyViewModel);

    TenancyViewModel delete(TenancyViewModel tenancyViewModel);

    TenancyViewModel findById(String tenantId);

    List<TenancyViewModel> findAll();

    String deleteAllCache();
}
