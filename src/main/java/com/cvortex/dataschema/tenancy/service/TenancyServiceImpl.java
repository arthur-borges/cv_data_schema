package com.cvortex.dataschema.tenancy.service;

import com.cvortex.dataschema.tenancy.model.Tenancy;
import com.cvortex.dataschema.tenancy.model.TenancyViewModel;
import com.cvortex.dataschema.tenancy.repository.TenacyTemplateRepository;
import com.cvortex.dataschema.tenancy.repository.TenancyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class TenancyServiceImpl implements TenancyService {

    private final TenancyRepository tenancyRepository;
    private final TenacyTemplateRepository tenacyTemplateRepository;

    public TenancyViewModel save(TenancyViewModel tenancyViewModel) {
        final String tenantId = getTenantId();
        Tenancy tenancy = this.tenancyRepository.save(Tenancy
                .builder()
                .id(tenantId)
                .name(tenancyViewModel.getName())
                .build());
        return TenancyViewModel.of(tenancy);
    }

    @Override
    public TenancyViewModel delete(TenancyViewModel tenancyViewModel) {
        return null;
    }

    @Override
    public TenancyViewModel update(TenancyViewModel tenancyViewModel) {
        return null;
    }

    @Cacheable(value = "tenancy", key = "#tenantId", unless = "#result == null")
    public TenancyViewModel findById(String tenantId) {
        log.info("Finding Tenancy by Id :{}", tenantId);
        return TenancyViewModel.of(this.tenancyRepository.findById(tenantId).orElse(null));
    }

    public List<TenancyViewModel> findAll() {
        return this.tenacyTemplateRepository.findAll().stream().map(TenancyViewModel::of).collect(Collectors.toList());
    }

    @Override
    @CacheEvict(allEntries = true, value = "tenancy")
    public String deleteAllCache() {
        log.info("Deleting Cache");
        return "Deleted Full Cache";
    }

    private String getTenantId() {
        return UUID.randomUUID().toString();
    }
}
