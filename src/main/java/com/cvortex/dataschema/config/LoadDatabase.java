package com.cvortex.dataschema.config;

import com.cvortex.dataschema.tenancy.model.TenancyViewModel;
import com.cvortex.dataschema.tenancy.service.TenancyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class LoadDatabase {

    private final TenancyService service;

    @Bean
    CommandLineRunner initDatabase() {
        return args -> {
            log.info("Preloading " + service.save(TenancyViewModel
                    .builder()
                    .name("Tenancy 1")
                    .build()));
            log.info("Preloading " + service.save(TenancyViewModel
                    .builder()
                    .name("Tenancy 2")
                    .build()));
        };
    }
}
